﻿using UnityEngine;
using System.Collections;

public class MegamanController : MonoBehaviour 
{
	public Animator animController;
	public float velocidadMovimiento = 3;
	public LayerMask groudLayer;
	public Transform groudPivot;

	private Rigidbody2D rig2D;
	private SpriteRenderer spRender;

	private bool grouded = false;

	void Awake () 
	{
		rig2D = GetComponent<Rigidbody2D>();
		spRender = GetComponent<SpriteRenderer>();
	}
	

	void Update()
	{
		if( Input.GetKey( KeyCode.Return ) )
			animController.SetBool( "Disparando", true);
		else
			animController.SetBool( "Disparando", false);

		if( Input.GetKeyDown( KeyCode.Space ) && grouded )
			rig2D.AddForce( Vector2.up * 20, ForceMode2D.Impulse );
	}

	void FixedUpdate () 
	{
		float axisX = Input.GetAxis("Horizontal");

		grouded = Physics2D.OverlapCircle( groudPivot.position, 0.1f, groudLayer);

		rig2D.velocity = new Vector2(velocidadMovimiento * axisX, rig2D.velocity.y);

		if( axisX != 0 )
			spRender.flipX = Mathf.Sign(axisX) == -1;
		
		animController.SetFloat("VelocidadX", Mathf.Abs(rig2D.velocity.x));
		animController.SetFloat("VelocidadY", rig2D.velocity.y);
		animController.SetBool("Grounded", grouded);
	}

}
