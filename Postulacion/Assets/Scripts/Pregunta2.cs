﻿using UnityEngine;
using System.Collections;

public class Pregunta2 : MonoBehaviour 
{
	public Transform[] rectangulos;

	void Start () 
	{
		bool colision = VerificarColision3Rectangulos((Vector2)rectangulos[0].position, rectangulos[0].localScale.x, rectangulos[0].localScale.y,
			(Vector2)rectangulos[1].position, rectangulos[1].localScale.x, rectangulos[1].localScale.y,
			(Vector2)rectangulos[2].position, rectangulos[2].localScale.x, rectangulos[2].localScale.y);

		Debug.Log(colision);
	}

	bool VerificarColisionRectangulos( Vector2 p1, float w1, float h1, Vector2 p2, float w2, float h2 )
	{
		return p1.x+w1/2 > p2.x-w2/2 &&
			   p1.x-w1/2 < p2.x+w2/2 &&
			   p1.y+h1/2 > p2.y-h2/2 &&
			   p1.y-h1/2 < p2.y+h2/2;
	}

	bool VerificarColision3Rectangulos(  Vector2 p1, float w1, float h1, Vector2 p2, float w2, float h2, Vector2 p3, float w3, float h3)
	{
		return VerificarColisionRectangulos( p1,w1,h1, p2,w2,h2 ) && 
			   VerificarColisionRectangulos( p2,w2,h2, p3,w3,h3 ) && 
			   VerificarColisionRectangulos( p3,w3,h3, p1,w1,h1 );
	}

}
