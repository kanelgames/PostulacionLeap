﻿using UnityEngine;
using System.Collections;

public class Pregunta3 : MonoBehaviour
{
	public int numPuntos = 5;
	public float radio = 5;
	public GameObject puntoPref;

	private ArrayList puntos;

	// Use this for initialization
	void Start () 
	{
		SemicircunferenciaRandom(numPuntos, this.transform.position, radio);
		InstanciarPuntos();
	}
	
	void SemicircunferenciaRandom( int cant , Vector3 p, float r)
	{
		puntos = new ArrayList();

		while ( cant>0 )
		{
			float angulo = Random.value * Mathf.PI;
			float radioRandom = Random.value * r;

			Vector3 punto = p + new Vector3(Mathf.Cos(angulo), Mathf.Sin(angulo)) * radioRandom;
			puntos.Add( punto );

			cant--;
		}
	}

	void InstanciarPuntos()
	{
		for(int i=0 ; i<puntos.Count ; i++)
			Instantiate( puntoPref, (Vector3)puntos[i], Quaternion.identity );
	}

	void OnDrawGizmos()
	{
		Vector3 posInicial = this.transform.position;

		for( int i=1; i<=180 ; i++)
		{
			Vector3 origen = new Vector3( Mathf.Cos((i-1)*Mathf.Deg2Rad),Mathf.Sin((i-1)*Mathf.Deg2Rad),0) * radio;
			Vector3 fin = new Vector3( Mathf.Cos(i*Mathf.Deg2Rad),Mathf.Sin(i*Mathf.Deg2Rad),0) * radio;
			Gizmos.DrawLine( origen + posInicial, fin + posInicial);
		}
	}
}
