﻿using UnityEngine;
using System.Collections;

public class Pregunta1 : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		for( int i=0; i<30 ; i++ )
		{
			Debug.Log("INPUT : "+ i);
			Debug.Log( EsNumeroPrimo(i) );
		}
	}


	bool EsNumeroPrimo (int n)
	{
		float i = Mathf.Sqrt(n);
		float j = Mathf.Round(i);
		float x = 2;

		while(x <= j)
		{
			if( (n % x) == 0 ) 
				return false;
			
			else x++; 
		}
		return true; 
	}
}
